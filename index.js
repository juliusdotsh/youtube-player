var videos = [];
var counter = 0;
var counterTitle = 0;
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;
var slider;
var volume;
var key = "Your Key";
$(function(){
 slider = $("#slider").slider({
  min: 0,
  max: 0,
  value: 0,
  range: "min",
  animate: true,
  slide: function(event, ui){
    player.seekTo(ui.value,true);
  }
});
  volume = $("#volume").slider({
    min: 0,
    max: 100,
    range: "min",
    animate: true,
    slide: function(event, ui){
      player.setVolume(ui.value);
    }
  });
});
function createPlayer(video){
  player = new YT.Player('player', {
    height: '0',
    width: '0',
    videoId: video,
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange,
      'onError': onPlayerError
    }
  });
}
function onPlayerError(event){
  $('#errorMessage').html("<strong>Attention!</strong> this track is not playable through Youtube Player error code:<strong>"+ event.data + "</strong>");
  $('#error').show();
  remove(counter);
  displayVideos();
}
function search(){
  $("#searchResult").html("");
  var val = $("#search").val();
  $.get("https://www.googleapis.com/youtube/v3/search?key="+ key +"&part=snippet&type=video,playlist&q="+ val, function(data,status){
    if(status=="success"){
      for(var i = 0; i < data.items.length; i++){
        if(data.items[i].id.kind == "youtube#video"){
          $("#searchResult").html($("#searchResult").html()+"<div class='columns is-flex'><div class='column is-11'>"+ data.items[i].snippet.channelTitle +" - "+ data.items[i].snippet.title +"</div><div class='column is-1'><button class='button is-primary' onclick='addVideo("+'"'+ data.items[i].id.videoId +'"'+")'><i class='fa fa-plus' aria-hidden='true'></i></button></div></div>");
        }else{
          $("#searchResult").html($("#searchResult").html()+"<div class='columns is-flex'><div class='column is-11'>Playlist:"+ data.items[i].snippet.channelTitle +" - "+data.items[i].snippet.title+"</div><div class='column is-1'><button class='button is-primary' onclick='addPlaylist("+'"'+ data.items[i].id.playlistId +'"'+")')'><i class='fa fa-plus' aria-hidden='true'></i></button></div></div>");
        }
      }
    }else{
      console.log(data + ": " + status);
      alert("Couldn't connect to the server");
    }
  });
}
function skipTo(number){
  counter = number;
  player.loadVideoById(videos[counter]);
}
function skipPrevious(){
  if(counter != 0){
    hideError();
    counter--;
    player.loadVideoById(videos[counter]);
  }
}
function addVideo(){
  addVideo($("#search").val().split("?v=")[1]);
}
function goUp(number){
  var x = videos[number];
  videos[number] = videos[number-1];
  videos[number-1] = x;
  displayVideos();
}
function goDown(number){
  var x = videos[number];
  videos[number] = videos[number+1];
  videos[number+1] = x;
  displayVideos();
}
function addPlaylist(video){
  $.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&key="+key+"&playlistId=" + video , function(data,status){
    var nextPageToken = data.nextPageToken;
    for(var i = 0; i < data.items.length; i++){
      if(player==null){
        videos[videos.length] = data.items[i].snippet.resourceId.videoId;
        createPlayer(data.items[i].snippet.resourceId.videoId);
      }else{
        if(typeof player.getPlayerState !== "undefined"){
          if(player.getPlayerState()==YT.PlayerState.ENDED|| player.getPlayerState()==-1){
            videos[videos.length] = data.items[i].snippet.resourceId.videoId;
            player.loadVideoById(videos[videos.length-1]);
            counter++;
            slider.slider("option","max", player.getDuration());
          }
        }
        videos[videos.length] = data.items[i].snippet.resourceId.videoId;
      }
    }
    if(data.pageInfo.totalResults > 50){
      while (nextPageToken != null) {
        $.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&key="+key+"&pageToken="+nextPageToken+"&playlistId="+video, function(Data,status){
          console.log(nextPageToken);
          for(var i = 0; i < Data.items.length; i++){
            if(player==null){
              videos[videos.length] = Data.items[i].snippet.resourceId.videoId;
              createPlayer(Data.items[i].snippet.resourceId.videoId);
            }else{
              if(typeof player.getPlayerState !== "undefined"){
                if(player.getPlayerState()==YT.PlayerState.ENDED|| player.getPlayerState()==-1){
                  videos[videos.length] = Data.items[i].snippet.resourceId.videoId;
                  player.loadVideoById(videos[videos.length-1]);
                  counter++;
                  slider.slider("option","max", player.getDuration());
                }
              }
              videos[videos.length] = Data.items[i].snippet.resourceId.videoId;
            }
          }
          nextPageToken = Data.nextPageToken;
        });
      }
    }
  });
  displayVideos();
}
function displayVideos(){
  $("#videos").html("");
  for (var i = 0; i < videos.length; i++) {
    counterTitle=0;
    displayVideo(videos[i]);
  }
}
function remove(number){
  videos.splice(number,1);
  displayVideos();
}
function displayVideo(video) {
  $.get("https://www.googleapis.com/youtube/v3/videos?id="+ video +"&key="+ key +"&part=snippet", function(data,status){
    var mainContent = '<div class="columns is-gapless"><div class="column is-9"><p onclick="skipTo('+ counterTitle +')">'+ data.items[0].snippet.channelTitle +' - '+  data.items[0].snippet.title +'</p></div>';
    var clear = '<button class="button is-danger" onclick="remove('+counterTitle+')"><i class="fa fa-minus"></i></button></div>';
    if(status=="success"){
      if(counterTitle == 0){
        $("#videos").html($("#videos").html() + mainContent +"<div class='column'><button class='button is-primary' onclick='goDown("+counterTitle+")'><i class='fa fa-arrow-down'></i></button>"+ clear);
      }else if(counterTitle == videos.length-1){
        $("#videos").html($("#videos").html() + mainContent +"<div class='column'><button class='button is-primary' onclick='goUp("+counterTitle+")'>"+'<i class="fa fa-arrow-up"></i></button>'+ clear);
      }else{
        $("#videos").html($("#videos").html() + mainContent +"<div class='column'><button class='button is-primary' onclick='goUp("+counterTitle+")'>"+'<i class="fa fa-arrow-down"></i></button><button class="button is-primary" onclick="goDown('+counterTitle+')"><i class="fa fa-arrow-down"></i></button>'+ clear);
      }
      counterTitle++;
    }else{
      console.log(data + ": " + status);
      alert("Couldn't connect to the server");
    }
  });

}
function addVideo(video){
  if(player==null){
    videos[videos.length] = video;
    createPlayer(video);
  }else{
    if(player.getPlayerState()==YT.PlayerState.ENDED|| player.getPlayerState()==-1){
      player.loadVideoById(video);
      videos[videos.length] = video;
      displayVideos();
      counter++;
      if(!player.getVideoData().title.contains(player.getVideoData().author)){
        $('#title').html(player.getVideoData().author +" - "+player.getVideoData().title);
      }
      slider.slider("option","max", player.getDuration());
    }else{
      videos[videos.length] = video;
    }
  }
  displayVideos();
}
function skipNext(){
  if(videos.length != 0 && counter+1 < videos.length){
    hideError();
    counter++;
    player.loadVideoById(videos[counter]);
  }
}
function onPlayerReady(event) {
  event.target.setPlaybackQuality("small");
  event.target.setVolume(100);
  if(!event.target.getVideoData().title.includes(event.target.getVideoData().author)){
    $('#title').html(event.target.getVideoData().author +" - "+ event.target.getVideoData().title);
  }
  slider.slider("option","max", event.target.getDuration());
  event.target.playVideo();
}
function onPlayerStateChange(event){
  if(event.data == YT.PlayerState.PLAYING){
    slider.slider("option","max", player.getDuration());
    volume.slider("option","value",player.getVolume());
    setInterval(function(){slider.slider("option","value",player.getCurrentTime())},1000);
    if(player.getVideoData().title != $('#title').html()){
      if(!player.getVideoData().title.includes(player.getVideoData().author)){
        $('#title').html(player.getVideoData().author +" - "+player.getVideoData().title);
      }else{
        $('#title').html(player.getVideoData().title);
      }
      slider.slider("option","max", player.getDuration());
    }
  }else if(player.getPlayerState() == YT.PlayerState.ENDED){
    if(videos.length != 0 && counter < videos.length-1){
      counter++;
      player.loadVideoById(videos[counter]);
    }
  }
}
function playVideo() {
  player.playVideo();
}
function stopVideo() {
  player.pauseVideo();
}
function hideError() {
  $('#error').hide();
}
